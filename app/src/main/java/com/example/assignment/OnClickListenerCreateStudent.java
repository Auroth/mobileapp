package com.example.assignment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Toast;

import static android.content.ContentValues.TAG;


public class OnClickListenerCreateStudent implements View.OnClickListener {

    private final Context mContext;

    /* public String TAG = "MainActivity";

    private LoginActivity.UserLoginTask mAuthTask = null; */

    public OnClickListenerCreateStudent(Context context){
        mContext = context;
    }

    @Override
    public void onClick(View view) {
        final Context context = mContext; //view.getRootView().getContext();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View formElementsView = inflater.inflate(R.layout.student_input_form, null, false);
        final EditText editTextStudentFirstname = (EditText) formElementsView.findViewById(R.id.editTextStudentFirstname);
        final EditText editTextStudentEmail = (EditText) formElementsView.findViewById(R.id.editTextStudentEmail);
        new AlertDialog.Builder(context)
                .setView(formElementsView)
                .setTitle("Create Student")
                .setPositiveButton("Add",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String studentFirstname = editTextStudentFirstname.getText().toString();
                                String studentEmail = editTextStudentEmail.getText().toString();
                                ObjectStudent objectStudent = new ObjectStudent();
                                objectStudent.firstname = studentFirstname;
                                objectStudent.email = studentEmail;

                                /* Log.e(TAG, "attemptCreateStudent");

                                if (mAuthTask != null) {
                                    return;
                                }

                                editTextStudentFirstname.setError(null);
                                editTextStudentEmail.setError(null);

                                boolean cancel = false;
                                View focusView = null;

                                if (!TextUtils.isEmpty(studentFirstname) && !isFirstNameValid(studentFirstname)) {
                                    editTextStudentFirstname.setError(getString(R.string.error_invalid_password));
                                    focusView = editTextStudentFirstname;
                                    cancel = true;
                                }

                                if (TextUtils.isEmpty(studentEmail)) {
                                    editTextStudentEmail.setError(getString(R.string.error_field_required));
                                    focusView = editTextStudentEmail;
                                    cancel = true;
                                } else if (!isEmailValid(studentEmail)) {
                                    editTextStudentEmail.setError(getString(R.string.error_invalid_email));
                                    focusView = editTextStudentEmail;
                                    cancel = true;
                                }

                                if (cancel) {
                                    focusView.requestFocus();
                                } else {

                                    Log.e(TAG, "Async started.");
                                    showProgress(true);
                                    mAuthTask = new MainActivity.UserLoginTask(studentFirstname, studentEmail);
                                    mAuthTask.execute((Void) null);
                                }

                                boolean createSuccessful = new TableControllerStudent(context).create(objectStudent);
                                if (createSuccessful) {
                                    Toast.makeText(context, "Student information was saved.", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(context, "Unable to save student information.", Toast.LENGTH_SHORT).show();
                                }

                                boolean isFirstNameValid(String firstname) {
                                    if()
                                    return firstname.;
                                }

                                boolean isEmailValid(String email) {
                                    return email.contains("@");
                                } */



                                ((MainActivity) context).countRecords();
                                ((MainActivity) context).readRecords();
                                dialog.cancel();
                            }
                        }).show();

    }
}
